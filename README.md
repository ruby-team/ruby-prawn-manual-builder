# Prawn::ManualBuilder

This library is used by Prawn to generate its self-documenting manual,
and could be used by third-party Prawn extensions to create manuals
for their functionality as well.

It is in a very early stage of maintenance, and is not yet officially
supported for use outside of Prawn. However, you're welcome to use
it at your own risk!

Prawn::ManualBuilder was initially developed as an internal tool for
Prawn by Felipe Doria, and is now maintained by Gregory Brown.
